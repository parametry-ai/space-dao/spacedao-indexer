Space DAO Indexer
=================


Space DAO indexer is a python based scanner that reorganize the Algorand Indexer
information into Space DAO semantics and provides a dedicated dashboard for it.


License
-------

This work is licensed under the LGPL-version-3-or-later.


