# SPDX-License-Identifier: LGPL-3.0-or-later
""" Init file for spacedao-indexer
"""
from ._version import get_versions

__version__ = get_versions()['version']
del get_versions

