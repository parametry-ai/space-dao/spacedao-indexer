# SPDX-License-Identifier: LGPL-3.0-or-later
"""
    Main command line program
"""

import click
from spaecdao-indexer import __version__
from spacedao-indexer import scan


@click.version_option(version=__version__)
@click.group()
def cli():
    """
    Tool for analyzing satellite telemetry
    """
    return


@click.command('scan',
               context_settings={"ignore_unknown_options": True},
               short_help='Scan and show related Space DAO information')
@click.argument('block_nbr', nargs=1, required=False)
def cli_scan(block_nbr):
    """
        Scan blockat number block_nbr or scan last 10 blocks
    """
    scan(block_nbr)
    return

